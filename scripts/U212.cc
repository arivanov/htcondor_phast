#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "G3part.h"
#include "PaAlgo.h"
#include "PaMetaDB.h"

// Longitudinal double-spin asymmetry in exlusive rho production

void UserEvent212(PaEvent &e)
{
    const double M_mu = G3partMass[5];
    const double M_pion = G3partMass[8];
    const double M_K0 = G3partMass[16];
    const double M_KS = G3partMass[61];
    const double M_K = G3partMass[11];
    const double M_rho = G3partMass[63];
    const double M_phi = G3partMass[91];
    const double M_proton = G3partMass[14];

    static vector<TH1D *> h1;
    static TTree *tree(NULL);

    // ---- Events
    static int Event, Run, Spill, Target;
    static int NVertex, NParticle, NTrack, NOutP;
    static int year, month, day, hour, min, sec;

    // ---------- characteristics vertices --------------------------
    static int fTM;
    static double vX, vY, vZ;
    static double vdX, vdY, vdZ;

    // ---------- characteristics beam muon---------------------------
    static TLorentzVector lvmu0, lvmu1, lvq;
    static double Ebeam, Escat, SigmaBeam;

    static int Qbeam;

    static double Depolf, dDepolf, Depolf2, dDepolf2;
    static float Dilutf, dDilutf;
    static double weight;
    static float R, dR;

    static double Q2, Xbj, y, W, nu;

    static vector<float> poll;
    static double polT, polTmSol, BeamPol;

    // ------------- hadron -----------------------
    static TLorentzVector lvrho0, lvh1, lvh2;
    double Mrho0, Pt2rho0;
    double Mmiss;

    static bool first(true);
    if (first)
    {

        tree = new TTree("rho0", "");

        tree->Branch("Event", &Event, "Event/I");
        tree->Branch("Run", &Run, "Run/I");
        tree->Branch("Spill", &Spill, "Spill/I");
        tree->Branch("Target", &Target, "Target/I");
        tree->Branch("fTM", &fTM, "fTM/I");

        tree->Branch("NParticle", &NParticle, "NParticle/I");
        tree->Branch("NTrack", &NTrack, "NTrack/I");
        tree->Branch("NVertex", &NVertex, "NVertex/I");

        tree->Branch("year", &year, "year/I");
        tree->Branch("month", &month, "month/I");
        tree->Branch("day", &day, "day/I");
        tree->Branch("hour", &hour, "hour/I");
        tree->Branch("min", &min, "min/I");
        tree->Branch("sec", &sec, "sec/I");

        tree->Branch("vX", &vX, "vX/D");
        tree->Branch("vdX", &vdX, "vdX/D");
        tree->Branch("vY", &vY, "vY/D");
        tree->Branch("vdY", &vdY, "vdY/D");
        tree->Branch("vZ", &vZ, "vZ/D");
        tree->Branch("vdZ", &vdZ, "vdZ/D");

        tree->Branch("lvmu0", "TLorentzVector", &lvmu0);
        tree->Branch("lvmu1", "TLorentzVector", &lvmu1);
        tree->Branch("lvq", "TLorentzVector", &lvq);

        tree->Branch("Ebeam", &Ebeam, "Ebeam/D");
        tree->Branch("Escat", &Escat, "Escat/D");
        tree->Branch("SigmaBeam", &SigmaBeam, "SigmaBeam/D");
        tree->Branch("Qbeam", &Qbeam, "Qbeam/I");

        tree->Branch("Xbj", &Xbj, "Xbj/D");
        tree->Branch("y", &y, "y/D");
        tree->Branch("Q2", &Q2, "Q2/D");
        tree->Branch("W", &W, "W/D");
        tree->Branch("nu", &nu, "nu/D");

        tree->Branch("polT", &polT, "polT/D");
        tree->Branch("polTmSol", &polTmSol, "polTmSol/D");

        tree->Branch("BeamPol", &BeamPol, "BeamPol/D");

        tree->Branch("Depolf", &Depolf, "Depolf/D");
        tree->Branch("dDepolf", &dDepolf, "dDepolf/D");

        tree->Branch("R", &R, "R/F");
        tree->Branch("dR", &dR, "dR/F");

        tree->Branch("Dilutf", &Dilutf, "Dilutf/F");
        tree->Branch("dDilutf", &dDilutf, "dDilutf/F");
        tree->Branch("weight", &weight, "weight/D");

        tree->Branch("lvh1", "TLorentzVector", &lvh1);
        tree->Branch("lvh2", "TLorentzVector", &lvh2);

        tree->Branch("lvrho0", "TLorentzVector", &lvrho0);

        tree->Branch("Mrho0", &Mrho0, "Mrho0/D");
        tree->Branch("Mmiss", &Mmiss, "Mmiss/D");
        tree->Branch("Pt2rho0", &Pt2rho0, "Pt2rho0/D");

        first = false;

        h1.push_back(new TH1D("h1rho", "event selection", 40, 0, 40));
    }
    h1[0]->Fill(0);
    e.Date(year, month, day, hour, min, sec);

    Run = e.RunNum();
    Event = e.UniqueEvNum();
    Spill = e.SpillNum();
    NParticle = e.NParticle();
    NTrack = e.NTrack();
    NVertex = e.NVertex();

    fTM = e.TrigMask();
    fTM = (fTM & 2047);

    if (Run >= 20018 && Run <= 23449)
    {
        year = 2002;
    };

    if (Run >= 27599 && Run <= 32236)
    {
        year = 2003;
    };

    // ======================================================
    // ==== Vertex -----------------------`
    const int iBPV = e.iBestPrimaryVertex();
    if (iBPV == -1)
        return;
    h1[0]->Fill(1);

    const PaVertex &v = e.vVertex(iBPV);
    if (!v.IsPrimary())
        return;
    h1[0]->Fill(2);

    // ======================================================
    // ==== Muon' --------------------
    int imu_scat = v.iMuPrim(true, true, true, true, 15);
    // int imu_scat = v.iMuPrim();

    if (imu_scat == -1)
        return;
    h1[0]->Fill(3);

    const PaParticle &Mu_scat = e.vParticle(imu_scat);
    const PaTPar &ParamMu_scat = Mu_scat.ParInVtx(iBPV);
    lvmu1 = ParamMu_scat.LzVec(M_mu);
    const PaTrack &fTrMuScat = e.vTrack(Mu_scat.iTrack());

    if (!fTrMuScat.HasMom())
        return;
    h1[0]->Fill(4);

    if (!Mu_scat.IsMuPrim())
        return;
    h1[0]->Fill(5);

    // ======================================================
    // ==== Beam ------------------------

    int imu_beam = v.InParticle();
    if (imu_beam == -1)
        return;
    h1[0]->Fill(6);

    const PaParticle &Mu_beam = e.vParticle(imu_beam);
    const PaTPar &ParamMu_beam = Mu_beam.ParInVtx(iBPV);
    lvmu0 = ParamMu_beam.LzVec(M_mu);
    const PaTrack &fTrMubeam = e.vTrack(Mu_beam.iTrack());

    if (!fTrMubeam.HasMom())
        return;
    h1[0]->Fill(7);

    if (!Mu_beam.IsBeam())
        return;
    h1[0]->Fill(8);

    // ======================================================
    // ==== no pT in DB------------------------
    if (!PaMetaDB::Ref().TargetSpinZproj(Run, poll))
        return;
    h1[0]->Fill(9);

    // ======================================================
    // ==== Target and Cells

    // ------------------------------------
    if (!PaAlgo::CrossCells(ParamMu_beam, Run))
        return;
    h1[0]->Fill(10);

    // ------------------------------------
    if (year == 2002 || year == 2003 || year == 2004)
    {
        // --- 2002
        // xU = -0.2; yU = 0.1; zU_1 = -100; zU_2 = -40; xD = -0.3; yD = -0.15;zD_1 = -30; zD_2 = 30; R = 1.4; yCUT = 1;

        if (!(PaAlgo::InTarget(ParamMu_beam, 'U', Run)) && !(PaAlgo::InTarget(ParamMu_beam, 'D', Run)))
            return;
    }
    if (year == 2006 || year == 2007 || year == 2011)
    {

        if (!(PaAlgo::InTarget(ParamMu_beam, 'U', Run)) && !(PaAlgo::InTarget(ParamMu_beam, 'D', Run)) && !(PaAlgo::InTarget(ParamMu_beam, 'C', Run)))
            return;
    }

    h1[0]->Fill(11);

    // ===================================================================================
    // ===================================================================================
    TLorentzVector lvproton(0., 0., 0., M_proton);

    NOutP = v.NOutParticles();

    vX = v.X();
    vY = v.Y();
    vZ = v.Z();
    vdX = v.Cov(0);
    vdY = v.Cov(2);
    vdZ = v.Cov(5);

    lvq = lvmu0 - lvmu1;

    Ebeam = lvmu0.E();
    Escat = lvmu1.E();

    Xbj = PaAlgo::xbj(lvmu0, lvmu1);
    Q2 = PaAlgo::Q2(lvmu0, lvmu1);
    W = sqrt(PaAlgo::W2(lvmu0, lvmu1));
    y = (lvmu0.E() - lvmu1.E()) / lvmu0.E();
    nu = (lvmu0.E() - lvmu1.E());

    // ==== Number outgoing particles ------------------------------------
    if (v.NOutParticles() != 3)
        return;
    h1[0]->Fill(12);
    // -------------

    int ipart1 = v.iOutParticle(0);
    int ipart2 = v.iOutParticle(1);
    int ipart3 = v.iOutParticle(2);

    int ip1, ip2;
    ip1 = -1;
    ip2 = -1;

    if (ipart1 == imu_scat)
    {
        ip1 = ipart2;
        ip2 = ipart3;
    }
    if (ipart2 == imu_scat)
    {
        ip1 = ipart1;
        ip2 = ipart3;
    }
    if (ipart3 == imu_scat)
    {
        ip1 = ipart1;
        ip2 = ipart2;
    }

    PaParticle p1 = e.vParticle(ip1);
    PaParticle p2 = e.vParticle(ip2);

    if (p1.NFitPar() == 0 || p2.NFitPar() == 0)
        return;
    h1[0]->Fill(13);

    if (p1.Q() == p2.Q())
        return;
    h1[0]->Fill(14);

    if (p1.Q() < p2.Q())
    {
        swap(p1, p2);
    }

    const PaTPar &Par_p1 = p1.ParInVtx(iBPV);
    const PaTPar &Par_p2 = p2.ParInVtx(iBPV);
    int Ntrack_p1 = p1.iTrack();
    int Ntrack_p2 = p2.iTrack();

    if (Ntrack_p1 < 0)
        return;
    if (Ntrack_p2 < 0)
        return;
    h1[0]->Fill(15);

    const PaTrack &Track_p1 = e.vTrack(Ntrack_p1);
    const PaTrack &Track_p2 = e.vTrack(Ntrack_p2);

    if (!Track_p1.HasMom())
        return;
    if (!Track_p2.HasMom())
        return;
    h1[0]->Fill(16);

    float zlast1 = Track_p1.ZLast();
    float zlast2 = Track_p2.ZLast();

    float zfirst1 = Track_p1.ZFirst();
    float zfirst2 = Track_p2.ZFirst();

    if (zlast1 > 3300. || zlast2 > 3300.)
        return;
    h1[0]->Fill(17);

    if (zlast1 < 350. || zlast2 < 350.)
        return;
    h1[0]->Fill(18);

    if (zfirst1 > 350. || zfirst2 > 350.)
        return;
    h1[0]->Fill(19);

    if (Track_p1.XX0() > 10. || Track_p2.XX0() > 10.)
        return;
    h1[0]->Fill(20);

    if (fTrMuScat.XX0() < 30.)
        return;
    h1[0]->Fill(21);

    bool yokeSM2 = (fTrMuScat.CrossYokeSM2() || Track_p1.CrossYokeSM2() || Track_p2.CrossYokeSM2());
    if (yokeSM2)
        return;
    h1[0]->Fill(22);

    //----------------

    if ((fTrMubeam.Chi2tot() / fTrMubeam.Ndf()) > 10.)
        return;
    if ((fTrMuScat.Chi2tot() / fTrMuScat.Ndf()) > 10.)
        return;
    if ((Track_p1.Chi2tot() / Track_p1.Ndf()) > 10.)
        return;
    if ((Track_p2.Chi2tot() / Track_p2.Ndf()) > 10.)
        return;
    h1[0]->Fill(23);

    // calculate pi+pi- invariant mass
    lvh1 = p1.ParInVtx(iBPV).LzVec(M_pion); // assign pi mass to particle #1
    lvh2 = p2.ParInVtx(iBPV).LzVec(M_pion); // assign pi mass to particle #2
    lvrho0 = lvh2 + lvh1;                   // Lorentz vector of pi+pi- system

    Mrho0 = lvrho0.M();

    TLorentzVector t = lvq - lvrho0; // 4-momentum transfer to the target

    // Pt2rho = v3rho.Pt(v3photon);

    Pt2rho0 = t.Pt() * t.Pt();

    // cout << vrho.Pt() << "  " << Pt2rho << endl;

    // have a look on seahorseother particles in the vertex
    PaTPar MaxMomParam;
    double maxP = 0;
    int ih = -1;
    TLorentzVector LzVecOut(0, 0, 0, 0); // for sum 4-vectors of all created particles
    for (int j = 0; j < v.NOutParticles(); j++)
    {                               // loop over outgoing particles of the vertex
        int ip = v.iOutParticle(j); // index of outgoing particle
        const PaParticle &p = e.vParticle(ip);
        if (p.IsMuPrim())
            continue; // skip mu'
        const PaTPar &param = p.ParInVtx(iBPV);
        LzVecOut += param.LzVec(M_pion); // add 4-vector to sum
        if (param.Mom() > maxP)
        { // search particle with max. momentum
            maxP = param.Mom();
            ih = ip;
            MaxMomParam = param;
        };
    }; // end of loop over outgoing particles

    // double Mx2 = (lvmu0 + lvproton - (lvmu1 + LzVecOut)).M2();
    //  Mmiss = (Mx2 - M_proton * M_proton) / (2 * M_proton);
    //     double Emiss = (Mrho0 * Mrho0 - M_proton * M_proton) / (2 * M_proton);

    TLorentzVector lvvertex = lvproton + lvq - lvrho0; // "missing" Lorentz vector for pi+pi- system
    Mmiss = lvvertex.M();
    double Mmiss2 = lvvertex.M2();
    double MXmiss = (Mmiss2 - M_proton * M_proton) / (2 * M_proton);

    if (nu < 30)
        return;
    h1[0]->Fill(24);

    if (Escat < 20)
        return;
    h1[0]->Fill(25);

    if (year == 2002 || year == 2003 || year == 2004 || year == 2006)
    {
        if (Pt2rho0 < 0.15)
            return;
    }
    if (year == 2007 || year == 2011)
    {
        if (Pt2rho0 < 0.1)
            return;
    }

    if (Pt2rho0 > 0.5)
        return;
    h1[0]->Fill(26);

    if (MXmiss < -2.5)
        return;
    if (MXmiss > 2.5)
        return;
    h1[0]->Fill(27);

    // peak of rho 770 Mev
    if (Mrho0 < 0.5)
        return;
    if (Mrho0 > 1.0)
        return;
    h1[0]->Fill(28);

    //  ==================================================
    // ==== Trigget ------------------------------------

    bool fTI = false;
    bool fSIT = false;
    int k = 0;

    if (year == 2002)
    {
        // -- Inclusive events --
        // 256  - inclMT
        // 8    - OT
        if ((fTM & 256) != 0 || (fTM & 8) != 0)
        {
            fTI = true;
            k = 1;
        }
        // -- Hadron events --
        // 1   - IT
        // 2   - MT
        // 4   - LT
        if (fTI == false && v.NOutParticles() >= 2)
        {
            if ((fTM & 1) != 0 || (fTM & 2) != 0 || (fTM & 4) != 0)
            {
                fSIT = true;
                k = 2;
            }
        }
    }

    if (year == 2003)
    {
        // -- Inclusive events --
        // 256  - inclMT
        // 8    - OT
        // 1024 - J/phi
        if ((fTM & 1024) != 0 || (fTM & 256) != 0 || (fTM & 8) != 0)
        {
            fTI = true;
            k = 1;
        }
        // -- Hadron events --
        // 1   - IT
        // 2   - MT
        // 4   - LT
        // 512 - OCT
        // 16  - CT
        if (fTI == false && v.NOutParticles() >= 2)
        {
            if ((fTM & 1) != 0 || (fTM & 2) != 0 || (fTM | 16) == 16 || (fTM & 4) != 0 || (fTM & 512) != 0)
            {
                fSIT = true;
                k = 2;
            }
        }
    }

    if (year == 2004)
    {
        // -- Inclusive events --
        // 256  - inclMT
        // 8    - OT
        // 1024 - J/phi
        if ((fTM & 1024) != 0 || (fTM & 256) != 0 || (fTM & 8) != 0)
        {
            fTI = true;
            k = 1;
        }
        // -- Hadron events --
        // 1   - IT
        // 2   - MT
        // 4   - LT
        // 512 - OCT
        // 16  - CT
        if (fTI == false && v.NOutParticles() >= 2)
        {
            if ((fTM & 1) != 0 || (fTM & 2) != 0 || (fTM | 16) == 16 || (fTM & 4) != 0 || (fTM & 512) != 0)
            {
                fSIT = true;
                k = 2;
            }
        }
    }

    if (year == 2006)
    {
        // -- Inclusive events --
        // 256  - inclMT
        // 8    - OT
        if ((fTM & 256) != 0 || (fTM & 8) != 0)
        {
            fTI = true;
            k = 1;
        }
        // -- Hadron events --
        // 1   - IT
        // 2   - MT
        // 4   - LT
        // 16  - CT
        if (fTI == false && v.NOutParticles() >= 2)
        {
            if ((fTM & 1) != 0 || (fTM & 2) != 0 || (fTM | 16) == 16 || (fTM & 4) != 0 )
            {
                fSIT = true;
                k = 2;
            }
        }
    }

    if (year == 2007)
    {

    
        // -- Inclusive events --
        // 256  - inclMT
        // 8    - OT
        if ((fTM & 256) != 0 || (fTM & 8) != 0)
        {
            fTI = true;
            k = 1;
        }
        // -- Hadron events --
        // 1   - IT
        // 2   - MT
        // 4   - LT
        // 16  - CT
        if (fTI == false && v.NOutParticles() >= 2)
        {
            if ((fTM & 1) != 0 || (fTM & 2) != 0 || (fTM | 16) == 16 || (fTM & 4) != 0)
            {
                fSIT = true;
                k = 2;
            }
        }
    }
    if (year == 2011)
    {
        // -- Inclusive events --
        // 256  - inclMT
        // 8    - OT 
        // 4    - LT

        if ((fTM & 256) != 0 || (fTM & 8) != 0 || (fTM & 4) != 0)
        {
            fTI = true;
        }

        // -- Hadron events --
        // 1   - IT
        // 2   - MT
        // 16  - CT
        // 512 - LAST
        if (fTI == false && v.NOutParticles() >= 2)
        {
            if ((fTM & 1) != 0 || (fTM & 2) != 0 || (fTM | 16) == 16 || (fTM & 512) != 0)
            {

                fSIT = true;
            }
        }
    }

    if (fTI == false && fSIT == false)
        return;

    /*
    // === |E-160|<20 -----------------------------------
    if (year == 2007)
    {
        if (fabs(lvmu0.E() - 160.) > 20.)
            return;
    }
    if (year == 2011)
    {
        if (fabs(lvmu0.E() - 200.) > 20.)
            return;
    }
    h1[0]->Fill(12);

    // === beam quality -----------------------------------

    if (year == 2007)
    {
        if (fabs(lvmu0.E() - 160.) < 0.0001)
            return;
    }
    if (year == 2011)
    {
        if (ParamMu_beam(5, 5) > 20E-9)
            return;
    }
    h1[0]->Fill(13);

    // ---------------------------------------------------
    if (e.Year() == 2006 || e.Year() == 2007)
    {
        if (Xbj <= 0.004 || Xbj > 0.7)
            return;
    }
    if (e.Year() == seahorse2011)
    {
        if (Xbj <= 0.0025 || Xbj > 0.7)
            return;
    }
    h1[0]->Fill(10);

    // ---------------------------------------------------
    if (Q2 < 1.)
        return;
    h1[0]->Fill(9);

    // -------------------Target--------------------------------
    if (y < 0.1 || y > 0.9)
        return;
    h1[0]->Fill(10);

    // ------------------------------------------------------
*/

    // ==================================================================================
    // =============================================9
    k = 1;
    Dilutf = dDilutf = 0;
    const PaSetup &setup = PaSetup::Ref();

    vector<float> polTmultSolenoidCurrentSing = setup.TargetPolarizations();

    if (year == 2002 || year == 2003 || year == 2004)
    {
        if (PaAlgo::InTarget(ParamMu_beam, 'U', Run))
        {
            Target = 1;
            PaAlgo::GetDilutionFactor((float)Xbj, (float)y, 'U', Run, k, Dilutf, dDilutf);
            polT = poll[0];
            polTmSol = polTmultSolenoidCurrentSing[0];
        }
        if (PaAlgo::InTarget(ParamMu_beam, 'D', Run))
        {
            Target = 2;
            PaAlgo::GetDilutionFactor((float)Xbj, (float)y, 'D', Run, k, Dilutf, dDilutf);
            polT = poll[1];
            polTmSol = polTmultSolenoidCurrentSing[1];
        }
    }

    if (year == 2006 || year == 2007 || year == 2011)
    {
        if (PaAlgo::InTarget(ParamMu_beam, 'U', Run))
        {
            Target = 1;
            PaAlgo::GetDilutionFactor((float)Xbj, (float)y, 'U', Run, k, Dilutf, dDilutf);
            polT = poll[0];
        }
        if (PaAlgo::InTarget(ParamMu_beam, 'C', Run))
        {
            Target = 2;
            PaAlgo::GetDilutionFactor((float)Xbj, (float)y, 'C', Run, k, Dilutf, dDilutf);
            polT = poll[1];
        }
        if (PaAlgo::InTarget(ParamMu_beam, 'D', Run))
        {
            Target = 3;
            PaAlgo::GetDilutionFactor((float)Xbj, (float)y, 'D', Run, k, Dilutf, dDilutf);
            polT = poll[2];
        }
    }

    Depolf = dDepolf = 0;
    PaAlgo::GetDepolarizationFactor((double)Q2, (double)Xbj, (double)y, Depolf, dDepolf, true);
    R = dR = 0;
    PaAlgo::GetR((double)Q2, (double)Xbj, R, dR, true);

    // -------------
    BeamPol = PaAlgo::GetBeamPol((float)ParamMu_beam.Mom(), year);
    weight = (float)BeamPol * (float)Depolf * (float)Dilutf;
    // -------------
    tree->Fill();
}
