#!/usr/bin/env python3

import argparse
import yaml
import json
import os
import sys
import re
import subprocess
import shutil
import time

def createParser():
    '''
    Read argv from command line
    '''

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", default="terminal", choices=["terminal", "htcondor"], type=str, help="run phast jobs")
    parser.add_argument("-c", default="config.yaml", type=str, help="file with configuration in YAML format: default file name is config.yaml")
    parser.add_argument("-t", action="store_false", help="test mode: without running jobs")
    parser.add_argument("-s", default=None, choices=["get", "status"], type=str, help="stage files" )


    return parser


def loadConfiguration(namefile):
    '''
    Read configuration in YAML format
    '''
    if not os.path.exists(namefile):
        sys.exit(f"file {namefile} does not exist!")

    config = yaml.load(open(namefile), Loader=yaml.Loader)

    print('-'*50)
    for key in config:
        print(key, " : ", config[key])
    print('-'*50)

    return config

def formFilesList(config):
    '''
    Form file list 
    '''
    file_list={}


    if config.get("PERIOD") != None:

        if '0' in config.get("PERIOD") :
            input = config.get("INPUT");
            file_list["0"] = subprocess.check_output(['xrdfs', config.get('EOS_CERN'), 'ls', input]).splitlines()
        else:
            for period in config.get("PERIOD"):

                if config.get("YEAR") != None:
            
                    if  "eos" in config.get("INPUT"):

                        input = config.get("INPUT") + "/"+ str(config.get("YEAR")) + "/oracle_dst/" + period + "/mDST/"

                        file_list[period] = subprocess.check_output(['xrdfs', config.get('EOS_CERN'), 'ls', input]).splitlines()

                    else:

                        if not os.path.exists(config.get("INPUT")):
                            sys.exit(f"path {config.get('INPUT')} does not exist!")
                else:
                    
                    if  "eos" in config.get("INPUT"):

                        input = config.get("INPUT") + "/" + period + "/mDST/" 

                        file_list[period] = subprocess.check_output(['xrdfs', config.get('EOS_CERN'), 'ls', input]).splitlines()

                    else:

                        if not os.path.exists(config.get("INPUT")):
                            sys.exit(f"path {config.get('INPUT')} does not exist!")
        
        
               
        
            
    return file_list


def selectFiles(config, file_list):


    selectfile_list = {}
    for period in config.get("PERIOD"):
        
        iperiod_list=[]
        for ifile in file_list.get(period):
            
            difile = ifile.decode()
          
            if not "root" in difile:
                continue
         

            if "0" != config.get("PERIOD"):
                mfile = re.match(r'mDST-(\d+)-(\d+-\d+).root\.?(\d*)', os.path.basename(difile))
                run, slot, part = mfile.group(1), mfile.group(2), mfile.group(3)
              
            flag_save=True
           

            if "0" not in config.get("PERIOD"):
              
                if slot != config.get("PERIOD")[period]:
                    continue
            
            print (difile)
            if config.get("FILE") != None:
                flag_save=False
                for ifile_just in config.get("FILE"):
                    if os.path.basename(difile) == ifile_just:
                        flag_save=True

            if config.get("RUN") != None:
                flag_save=False
                for irun in config.get("RUN"):
                    if int(run) == int(irun):
                        flag_save=True


            if config.get("RUNMIN") != None:
                if (int(run) < config.get("RUNMIN")):
                    continue
            if config.get("RUNMAX") != None:
                if (int(run) > config.get("RUNMAX")):
                    continue
        
            if flag_save: 
                iperiod_list.append(difile)

        selectfile_list[period]=iperiod_list


    return selectfile_list


def stageStatus(config, file_list):



    for period in config.get("PERIOD"):
        flag_status_true=0
        flag_status_false=0
        Nfiles=0

        for ifile in file_list.get(period):
            Nfiles+=1
            stager = subprocess.run(
                ['xrdfs', config.get('EOS_CERN'), 'query', 'prepare', '0', ifile],  stdout=subprocess.PIPE, encoding='utf-8')


            stager_json = json.loads(stager.stdout)
            #print(ifile, ": ", stager_json["responses"][0]["online"])
            
            if (stager_json["responses"][0]["online"] == False):
                flag_status_false += 1
            else:
                flag_status_true += 1
    
    
        print("Period=", period, " Number files=", Nfiles, " stage status -> ", "true:",
              flag_status_true, " false:", flag_status_false)

    sys.exit()


def stageGet(config, file_list):

    Nfiles=0

    for period in config.get("PERIOD"):
        
        for ifile in file_list.get(period):
            Nfiles+=1
            stager_get = subprocess.run(
            ['xrdfs', config.get('EOS_CERN'), 'prepare', '-s', ifile],  stdout=subprocess.PIPE, encoding='utf-8')

            print("staget_get: ", ifile, " - ", stager_get.stdout)


    print("Number files=", Nfiles)

    sys.exit()


# ---------------------------------
def phastMake(config):

    path=os.getcwd() + "/scripts"
    pwd=os.getcwd()
    if not os.path.exists(path):
        sys.exit(f"path {path} does not exist!")

    UserEventExist=False
    for ifile in os.listdir(path):

        if  str(config.get("USEREVENT")) in ifile:

            pathToPhast=str(config.get('EXE')) +"/user/" + ifile

            pathToUserEvent=path +"/"+ ifile

            shutil.copyfile(pathToUserEvent, pathToPhast )
     

            os.chdir(config.get('EXE'))
            subprocess.run("make")
            os.chdir(os.getcwd())
            UserEventExist=True
    
    
    if not UserEventExist:
        sys.exit(f"in path:{path} UserEvent{config.get('USEREVENT')} does not exist!") 
    os.chdir(pwd)

def getBadspillsList(config, period):

    if not os.path.exists(config.get("BADSPILLSLIST")):
        sys.exit(f"path {config.get('BADSPILLSLIST')} does not exist!")

    badSpillList="None"
    for ifile in os.listdir(config.get("BADSPILLSLIST")):
        if period in ifile:
            badSpillList=ifile

    if badSpillList == "None":
        sys.exit(f"in path:{config.get('BADSPILLSLIST')} badspilllist for {period} does not exist!")
    else:
        return badSpillList


def createJobsDirectories(config):

    if os.path.exists( os.getcwd() + "/" + config.get("NAME")):
        user_input = input(f"Do you want to do clean {config.get('NAME')} ? (y/[n])")

    for idir in ["logs", "subs", "jobs", "output"]:

        name_sub_folder = os.getcwd() + "/" + config.get("NAME") + "/" + idir

        if not os.path.exists(name_sub_folder):
            os.makedirs(name_sub_folder)
        else:
            if (user_input == "y"):
                shutil.rmtree(name_sub_folder)
                os.makedirs(name_sub_folder)    
            
                



def createOutputDirectories(config):


    if config.get("OUTPUT") == None:
        dir_output = os.getcwd() + "/" + config.get("NAME") + "/output" 
    else:
        dir_output = config.get("OUTPUT") + "/" + config.get("NAME") 

    user_input = input(f"Do you want to do clean {dir_output} ? (y/[n])")


    path_output =dir_output + "/" + str(config.get("YEAR"))        
    for period in config.get("PERIOD"):

        if  not os.path.exists(path_output + "/" + period):
            os.makedirs(path_output + "/" + period)
        else:
            if (user_input == "y"):
                shutil.rmtree(path_output + "/" + period)
                os.makedirs(path_output + "/" + period)



def sendHTcondorJobs(config, args, njobs):

    path_to_file_htcondor = os.getcwd() + "/" + config.get("NAME")
    name_file_htcondor = path_to_file_htcondor + "/" + config.get("NAME") + ".sub"

    file_htcondor = open(name_file_htcondor, 'w')
    file_htcondor.write("universe = vanilla\n")
    file_htcondor.write(
        f"executable = {path_to_file_htcondor}/jobs/job$(ProcId).sh\n")
    file_htcondor.write(
        f"error = {path_to_file_htcondor}/logs/job$(ProcId).err\n")
    file_htcondor.write(
        f"log = {path_to_file_htcondor}/logs/job$(ProcId).log\n")
    file_htcondor.write(
        f"output = {path_to_file_htcondor}/logs/job$(ProcId).out\n")
    file_htcondor.write("getenv = True\n")
    file_htcondor.write(f"+JobFlavour = \"{config.get('TIME')}\"\n")

    file_htcondor.write(f"BATCH_NAME = {config.get('NAME')}\n")


    coreUsage = 'Requirements = '
    if (config.get("CORES")) != None:
        for icore in range(1, config.get("CORES")+1):
            icoreUsage = f"(Name == \"slot{icore}@" + socket.gethostname() + "\")"
            if (icore != int(config.get('CORES'))):
                icoreUsage = icoreUsage + " || "
            coreUsage = coreUsage + icoreUsage
        file_htcondor.write(f"{coreUsage}\n")
    file_htcondor.write(f"queue {njobs}")

    file_htcondor.close()

    os.chmod(name_file_htcondor,  0o777)

    if  args.t:
        subprocess.run(["condor_submit", name_file_htcondor])


def sendTerminalJobs(config, njobs):

    path_to_file_bash = os.getcwd() + "/" + config.get("NAME") + "/jobs"

    if  args.t:
        for i in range(njobs+1):
         
            subprocess.run(["bash", path_to_file_bash + "/job" + str(i) + ".sh"])


def createSubJobs(config, ifile, period):
    
    path_to_file_bash = os.getcwd() + "/" + config.get("NAME") + "/subs"

    basefile=os.path.basename(ifile) 

    name_file_bash =  period + "_" + basefile

    file_bash = open(path_to_file_bash + "/" + name_file_bash + ".sh", 'w')
    file_bash.write("#!/usr/bin/env bash\n")
    file_bash.write(f"TMPDIR_JOB={config.get('TMPDIR')}\n")
    file_bash.write(
        f"PATH_TO_TMPDIR=$TMPDIR_JOB/{name_file_bash}\n")
    file_bash.write(f"mkdir -pv $PATH_TO_TMPDIR\n")
    file_bash.write(
        f"ln -sfv {config.get('EXE')}/phast $PATH_TO_TMPDIR/phast\n")
    file_bash.write(f"cd $PATH_TO_TMPDIR\n")

    if config.get('COPYFILE'):
        if  "eos" in config.get('INPUT'):
            file_bash.write(f"xrdcp {config.get('EOS_CERN')}/{ifile} $PATH_TO_TMPDIR/input_{basefile}\n")
        else:
            file_bash.write(f"cp -v {ifile} $PATH_TO_TMPDIR/input_{basefile}\n")


    if config.get("BADSPILLSLIST") is None:

        
        
        if config.get("COPYFILE"):
            file_bash.write(
                f"./phast -u {config.get('USEREVENT')} -h $PATH_TO_TMPDIR/output_{basefile} $PATH_TO_TMPDIR/input_{basefile}\n")
        else:
            if "eos" in config.get('INPUT'):
                file_bash.write(
                    f"./phast -u {config.get('USEREVENT')} -h $PATH_TO_TMPDIR/output_{basefile} {config.get('EOS_CERN')}/{ifile} \n")
            else:
                file_bash.write(
                    f"./phast -u {config.get('USEREVENT')} -h $PATH_TO_TMPDIR/output_{basefile} {ifile} \n")
        
    else:
        bad_spill_list = getBadspillsList(config, period)
        
        file_bash.write(
                f"cp {config.get('BADSPILLSLIST')}/{bad_spill_list} $PATH_TO_TMPDIR/{bad_spill_list} \n")

        if config.get("COPYFILE"):
            file_bash.write(
                f"./phast -u {config.get('USEREVENT')} -b {bad_spill_list} -h $PATH_TO_TMPDIR/output_{basefile} $PATH_TO_TMPDIR/input_{basefile}\n")
        else:
            if  "eos" in config.get('INPUT'):
                file_bash.write(
                    f"./phast -u {config.get('USEREVENT')} -b {bad_spill_list} -h $PATH_TO_TMPDIR/output_{basefile} {config.get('EOS_CERN')}/{ifile} \n")

            else:
                file_bash.write(
                    f"./phast -u {config.get('USEREVENT')} -b {bad_spill_list} -h $PATH_TO_TMPDIR/output_{basefile} {ifile} \n")



    if config.get("OUTPUT") == None:
        dir_output = os.getcwd() + "/" + config.get("NAME") + "/output" 
    else:
        dir_output = config.get("OUTPUT") + "/" + config.get("NAME") 

    path_output =dir_output + "/" + str(config.get("YEAR")) + "/" + period     


    file_bash.write(
        f"mv -v $PATH_TO_TMPDIR/output_{basefile}  {path_output}/{basefile} \n")

    file_bash.write(f"rm -rv $PATH_TO_TMPDIR\n")


    os.chmod(path_to_file_bash + "/" + name_file_bash + ".sh",  0o777)

    file_bash.close()

    os.chmod(path_to_file_bash + "/" + name_file_bash + ".sh",  0o777)


    return path_to_file_bash + "/" + name_file_bash + ".sh"

def createJobs(iflag, config, name_sub_jobs, ijobs):
    

    
    path_to_file_jobs = os.getcwd() + "/" + config.get("NAME") + "/jobs/job" + str(ijobs) + ".sh" 


    file = open(path_to_file_jobs, 'a')

    if iflag == 0:   
        file.write("#!/usr/bin/env bash\n")
    
    file.write(name_sub_jobs + "\n")
    file.close()

    os.chmod(path_to_file_jobs,  0o777)
    flag2=ijobs


if __name__ == '__main__':

    parser = createParser()
    args    = parser.parse_args()

    config  = loadConfiguration(args.c)

    
    file_list = formFilesList(config)

    selected_file_list = selectFiles(config, file_list)

    
    if (args.s == 'status'):
        stageStatus(config,selected_file_list )
 
    if (args.s == 'get'):
        stageGet(config,selected_file_list )

    
    phastMake(config)

    createJobsDirectories(config)
    createOutputDirectories(config)



    icount=0
    ijob=0
    for period in config.get("PERIOD"):

        for ifile in selected_file_list[period]:
            name_sub_jobs = createSubJobs(config, ifile, period)

            print (period, ":", ifile)
            
            createJobs(icount, config, name_sub_jobs, ijob)

            if icount < (config.get("SUBJOBS") -1):
                icount+=1
            else:
                icount=0
                ijob+=1

       

    if (args.r == "htcondor"):
        sendHTcondorJobs(config, args, ijob)

    if (args.r == "terminal"):
        sendTerminalJobs(config, ijob)
